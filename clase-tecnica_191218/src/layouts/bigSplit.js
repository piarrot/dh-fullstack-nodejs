// example Layout.js
import React from "react";

export default ({ children }) => (
  <div
    style={{
    //   flexGrow: "1",
      display: "flex",
    }}
  >
    <div style={{
        width: "60%"
    }}>
        {children[0]}
    </div>
    <div style={{
        width: "40%"
    }}>
        {children.slice(1)}
    </div>
  </div>
);
