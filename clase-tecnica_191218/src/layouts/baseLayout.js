// example Layout.js
import React from 'react'

export default ({ children }) => (
    <div
        style={{
            // flexGrow: "10",
            fontSize: "26px",
        }}
    >
        {children}
    </div>
)