function* generator(param) {
    console.log(">>start generator")
    yield param;
    yield param+=" is";
    yield param+=" awesome";
    console.log("<<end generator")
}
console.log("start")
var gen = generator("Nodejs");
console.log("generator created")

console.log(gen.next())
console.log(gen.next())
console.log(gen.next())
console.log(gen.next())
console.log(gen.next())
console.log(gen.next())
console.log(gen.next())
// console.log(gen.next().value)
// console.log(gen.next().value)
// console.log(gen.next().value)