const { Worker } = require("worker_threads");

jsonStr = "{\"nodejs\": \"is really cool\", \"workerThreads\":\"are great!\"}"

function parseJSONAsync(jsonStr){
    return new Promise((resolve,reject)=>{
        console.log("[PARENT] >> starting worker");
        const worker = new Worker("./src/jsdemo/workers-2.js",{
            workerData: jsonStr
        })
        console.log("[PARENT] >> worker started");
        worker.on("message",resolve); //resolve(datadelworker)
        worker.on("error",reject);
        worker.on("exit",(code)=>{
            if (code !== 0) {
                reject(new Error(`Worker stopped with exit code ${code}`))
            }
        })
    })
}

console.log("[PARENT] >> start");

parseJSONAsync(jsonStr).then((result)=>{
    console.log("[PARENT] result:",result);
})

console.log("[PARENT] << end");