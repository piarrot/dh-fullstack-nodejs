setTimeout(() => { 
    console.log(">> begin macrotask 1")
    queueMicrotask(() => { console.log("microtask 3") })
    queueMicrotask(() => { console.log("microtask 4") })
    console.log("<< end macrotask 1")
}, 0);
setTimeout(() => { 
    console.log(">> begin macrotask 2")
    console.log("<< end macrotask 2")
}, 0);

queueMicrotask(() => {console.log("microtask 1")})
queueMicrotask(() => {console.log("microtask 2")})

console.log("hola mundo");