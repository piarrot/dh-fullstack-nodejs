const {
  parentPort,
  workerData
} = require("worker_threads");

console.log("[Worker] >> started");

const jsonStr = workerData;



parentPort.postMessage(JSON.parse(jsonStr));


console.log("[Worker] << done");