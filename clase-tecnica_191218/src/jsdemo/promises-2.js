function funcWithPromise(data){
    return new Promise((resolve,reject)=>{
        console.log("promise >>> resolviendo")
        resolve(data)
        console.log("promise >>> resuelto")
    })
}

console.log(">> start");

funcWithPromise("hello world!").then((data)=>{
    console.log(data)
})

console.log(">> end");