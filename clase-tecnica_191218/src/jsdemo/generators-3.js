array = [1,2,3,4,5,6,7,8,9]

function* buscador(param) {
    for (let index = 0; index < array.length; index++) {
        const e = array[index];
        if (e == param) {
            return index;
        }
        yield;
    }
}

let b = buscador(9)

let found = function(value){
    console.log(value);
}

let i = setInterval(() => {
    let result = b.next()
    if (result.done){
        found(result.value)
        clearInterval(i)
    }
}, 0);